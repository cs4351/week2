#include "sample3D.h"
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "../samplefw/Grid3D.h"

struct Vertex
{
	GLfloat x,y,z;
	GLubyte r,g,b,a;
};

const float AXIS_EXTENT = 50.0f;
const int LINES_PER_HALFSPACE = 50;
const int GRID_LINES_PER_AXIS = LINES_PER_HALFSPACE*2; // ensure even number
const int TOTAL_GRID_LINES = GRID_LINES_PER_AXIS + GRID_LINES_PER_AXIS;

static const Vertex axesVertices[] = {
	// x-axis
	{ -AXIS_EXTENT, 0, 0, 255, 0, 0, 255 },
	{ AXIS_EXTENT, 0, 0, 255, 0, 0, 255 },
	// y-axis
	{ 0, -AXIS_EXTENT, 0, 0, 255, 0, 255 },
	{ 0, AXIS_EXTENT, 0, 0, 255, 0, 255 },
	// z-axis
	{ 0, 0, -AXIS_EXTENT, 0, 0, 255, 255 },
	{ 0, 0, AXIS_EXTENT, 0, 0, 255, 255 },
};

static const Vertex cubeVertices[] = {
	// Front
	{ -0.5f, -0.5f, 0.5f, 255, 0, 0, 255 },
	{ -0.5f,  0.5f, 0.5f, 255, 0, 0, 255 },
	{  0.5f,  0.5f, 0.5f, 255, 0, 0, 255 },
	{  0.5f,  0.5f, 0.5f, 255, 0, 0, 255 },
	{  0.5f, -0.5f, 0.5f, 255, 0, 0, 255 },
	{ -0.5f, -0.5f, 0.5f, 255, 0, 0, 255 },

	// Back
	{  0.5f,  0.5f,-0.5f, 128, 0, 0, 255 },
	{ -0.5f,  0.5f,-0.5f, 128, 0, 0, 255 },
	{ -0.5f, -0.5f,-0.5f, 128, 0, 0, 255 },
	{ -0.5f, -0.5f,-0.5f, 128, 0, 0, 255 },
	{  0.5f, -0.5f,-0.5f, 128, 0, 0, 255 },
	{  0.5f,  0.5f,-0.5f, 128, 0, 0, 255 },

	// Left
	{ -0.5f,  0.5f,-0.5f, 0, 255, 0, 255 },
	{ -0.5f,  0.5f, 0.5f, 0, 255, 0, 255 },
	{ -0.5f, -0.5f, 0.5f, 0, 255, 0, 255 },
	{ -0.5f, -0.5f, 0.5f, 0, 255, 0, 255 },
	{ -0.5f, -0.5f,-0.5f, 0, 255, 0, 255 },
	{ -0.5f,  0.5f,-0.5f, 0, 255, 0, 255 },

	// Right
	{  0.5f,  0.5f, 0.5f, 0, 128, 0, 255 },
	{  0.5f,  0.5f,-0.5f, 0, 128, 0, 255 },
	{  0.5f, -0.5f,-0.5f, 0, 128, 0, 255 },
	{  0.5f, -0.5f,-0.5f, 0, 128, 0, 255 },
	{  0.5f, -0.5f, 0.5f, 0, 128, 0, 255 },
	{  0.5f,  0.5f, 0.5f, 0, 128, 0, 255 },

	// Top
	{ -0.5f,  0.5f, 0.5f, 0, 0, 255, 255 },
	{ -0.5f,  0.5f,-0.5f, 0, 0, 255, 255 },
	{  0.5f,  0.5f,-0.5f, 0, 0, 255, 255 },
	{  0.5f,  0.5f,-0.5f, 0, 0, 255, 255 },
	{  0.5f,  0.5f, 0.5f, 0, 0, 255, 255 },
	{ -0.5f,  0.5f, 0.5f, 0, 0, 255, 255 },

	// Bottom
	{ -0.5f, -0.5f, 0.5f, 0, 0, 128, 255 },
	{  0.5f, -0.5f, 0.5f, 0, 0, 128, 255 },
	{  0.5f, -0.5f,-0.5f, 0, 0, 128, 255 },
	{  0.5f, -0.5f,-0.5f, 0, 0, 128, 255 },
	{ -0.5f, -0.5f,-0.5f, 0, 0, 128, 255 },
	{ -0.5f, -0.5f, 0.5f, 0, 0, 128, 255 },
};

static glm::mat4 mWorld;
static void TEST_MATRIX(float p_00, float p_01, float p_02, float p_03,
					    float p_10, float p_11, float p_12, float p_13,
				        float p_20, float p_21, float p_22, float p_23,
						float p_30, float p_31, float p_32, float p_33)
{
	mWorld[0][0] = p_00;
	mWorld[0][1] = p_01;
	mWorld[0][2] = p_02;
	mWorld[0][3] = p_03;

	mWorld[1][0] = p_10;
	mWorld[1][1] = p_11;
	mWorld[1][2] = p_12;
	mWorld[1][3] = p_13;

	mWorld[2][0] = p_20;
	mWorld[2][1] = p_21;
	mWorld[2][2] = p_22;
	mWorld[2][3] = p_23;

	mWorld[3][0] = p_30;
	mWorld[3][1] = p_31;
	mWorld[3][2] = p_32;
	mWorld[3][3] = p_33;
}

Sample3D::~Sample3D()
{
	printf("Destroying 3D Sample\n");
	delete m_pGrid;
	delete m_pDecl;
	wolf::BufferManager::DestroyBuffer(m_pVB);
	wolf::ProgramManager::DestroyProgram(m_pProgram);
}

void Sample3D::init()
{
    glEnable(GL_DEPTH_TEST);
        
    // Only init if not already done
    if(!m_pProgram)
    {
        m_pProgram = wolf::ProgramManager::CreateProgram("data/3d.vsh", "data/3d.fsh");
        m_pVB = wolf::BufferManager::CreateVertexBuffer(cubeVertices, sizeof(Vertex) * 6 * 3 * 2);

        m_pDecl = new wolf::VertexDeclaration();
        m_pDecl->Begin();
        m_pDecl->AppendAttribute(wolf::AT_Position, 3, wolf::CT_Float);
        m_pDecl->AppendAttribute(wolf::AT_Color, 4, wolf::CT_UByte);
        m_pDecl->SetVertexBuffer(m_pVB);
        m_pDecl->End();

        m_pGrid = new Grid3D(30);

        m_pOrbitCam = new OrbitCamera(m_pApp);
        m_pOrbitCam->focusOn(glm::vec3(-10.0f,-10.0f,-10.0f),glm::vec3(10.0f,10.0f,10.0f));
    }

    printf("Successfully initialized 3D Sample\n");
}

void Sample3D::update(float dt) 
{
    m_pOrbitCam->update(dt);
    m_pGrid->update(dt);
}

void Sample3D::render(int width, int height)
{
	glClearColor(0.3f, 0.3f, 0.3f, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDisable(GL_CULL_FACE);

	glm::mat4 mProj = m_pOrbitCam->getProjMatrix(width, height);
	glm::mat4 mView = m_pOrbitCam->getViewMatrix();

	// CHANGE HERE
	TEST_MATRIX( 1.0f, 0.0f, 0.0f, 0.0f,
			     0.0f, 1.0f, 0.0f, 0.0f,
				 0.0f, 0.0f, 1.0f, 0.0f,
				 0.0f, 0.0f, 0.0f, 1.0f );
	// END CHANGE HERE


    m_pGrid->render(mView, mProj);

	// Bind Uniforms
    m_pProgram->SetUniform("projection", mProj);
    m_pProgram->SetUniform("view", mView);
    m_pProgram->SetUniform("world", mWorld);    
	m_pProgram->Bind();
    
	// Set up source data
	m_pDecl->Bind();

    // Draw!
	glDrawArrays(GL_TRIANGLES, 0, 6 * 3 * 2);
}

