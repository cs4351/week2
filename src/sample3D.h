#pragma once
#include "../wolf/wolf.h"
#include "../samplefw/Sample.h"
#include "../samplefw/OrbitCamera.h"

class Grid3D;

class Sample3D: public Sample
{
public:
    Sample3D(wolf::App* pApp) : Sample(pApp, "3D Transforms") {}
    ~Sample3D();

    void init() override;
    void update(float dt) override;
    void render(int width, int height) override;

private:
    wolf::VertexBuffer* m_pVB = 0;
    wolf::VertexDeclaration* m_pDecl = 0;
    wolf::Program* m_pProgram = 0;
    OrbitCamera* m_pOrbitCam = nullptr;

    Grid3D* m_pGrid = nullptr;
};
